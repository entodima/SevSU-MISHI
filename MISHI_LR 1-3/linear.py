from graphviz import Digraph
import random


class Node:
	def __init__(self, id, telephone, abonent, child=None, parent=None):
		self.id = int(id)
		self.telephone = int(telephone)  # 1
		self.abonent = abonent  # 0
		self.child = child
		self.parent = parent

	def change_child(self, new_child):
		self.child = new_child


class Linear:
	nodes = [Node]

	def get_max_node_telephone(self):
		telephone = self.nodes[0].telephone
		for element in self.nodes:
			if element.telephone > telephone:
				telephone = element
		return telephone

	def get_min_node_telephone(self):
		telephone = self.nodes[0].telephone
		for element in self.nodes:
			if element.telephone < telephone:
				telephone = element
		return telephone

	def __init__(self, node):
		self.nodes[0] = node

	def get_root_node(self):
		temp = self.nodes[0]
		while temp.parent is not None:
			temp = temp.parent
		return temp

	def add_node(self, node):
		root_node = self.get_root_node()
		while True:
			if root_node.telephone >= node.telephone:
				if root_node.child is None:
					pass
				else:
					pass
			else:
				if root_node.parent is None:
					pass
				else:
					pass

		self.nodes.append(node)


	def search_node(self, node_telephone):
		pass

	def delete_node(self, node_telephone):
		node = self.search_node(node_telephone)


	def generate_graph(self, temp=False):
		dot = Digraph()
		for node in self.nodes:
			dot.node(str(node.id), str(node.telephone))
			if node.child is not None:
				dot.edge(str(node.id), str(node.child.id))
		if temp is True:
			dot.render('linear.gv', view=True)
		else:
			dot.render('linear.gv', renderer=None)
		del dot


file = open('Table 22.txt')
file_lines = file.readlines()
for i in range(1, len(file_lines)):
	file_lines[i - 1] = file_lines[i].replace('\n', '').split('\t')
	file_lines[i - 1].append('0')

print('Добавление нод', end='\n\n')
avl_tree = Linear(Node(0, file_lines[0][1], file_lines[0][0]))

# for i in range(1, len(file_lines)-1):
for i in range(1, 7):
	avl_tree.add_node(Node(i, file_lines[i][1], file_lines[i][0]))

avl_tree.generate_graph(True)