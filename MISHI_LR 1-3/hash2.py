import time


class Node:
    def __init__(self, telephone, abonent):
        self.telephone = int(telephone)  # 1
        self.abonent = abonent           # 0


class HashTable:
    nodes = dict()

    def add(self, line):
        if self.nodes.get(self.hash_func(line[1])) is None:
            self.nodes[self.hash_func(line[1])] = [line]
        else:
            self.nodes[self.hash_func(line[1])].append(line)

    def hash_func(self, val):
        val = int(val)
        return val % 100

    def find_del(self, val, delete = False):
        if self.nodes.get(self.hash_func(val)) is not None:
            for string in self.nodes.get(self.hash_func(val)):
                if string[1] == val:
                    if delete:
                        self.nodes.get(self.hash_func(val)).remove(string)
                    else:
                        return string

    def print(self):
        for node in self.nodes:
            print(node)
            for str in self.nodes[node]:
                print('\t' + (str[1]) + ' - ' + (str[0]))


file = open('Table 22.txt')
file_lines = file.readlines()
lines = []
''''''
for i in range(1, len(file_lines)):
    file_lines[i] = file_lines[i].replace('\n', '').split('\t')
    lines.append(file_lines[i])

hash_table = HashTable()
telephones = []

for line in lines:
    hash_table.add(line)
    telephones.append(line[1])

start_time = time.time()
#for i in range(8000):
#    id = random.randrange(0, len(telephones))
#    hash_table.find_delete(telephones[id], True)
end_time = time.time() - start_time

hash_table.print()

print('seconds: ' + str(end_time))

input()
hash_table.find_del('7192830', True)
hash_table.print()
print()
