from graphviz import Digraph
import random
import time
import copy


class Node:
    def __init__(self, telephone, abonent):
        self.telephone = int(telephone)  # 1
        self.abonent = abonent           # 0


class HashTable:
    nodes = dict()

    def add(self, line):
        node = Node(int(line[1]), line[0])
        hash_nmb = self.hash_func(node.telephone)

        if node.telephone in self.nodes:
            self.nodes[hash_nmb] = self.nodes[hash_nmb].append(node)
        else:
            self.nodes[hash_nmb] = [node]

    def find_delete(self, telephone, delete=False):
        for i in range(len(self.nodes)):
            for node in self.nodes[i]:
                if node.telephone == telephone:
                    if delete is True:  # удаление
                        print(telephone)
                        self.nodes[i].remove(node)
                        return None
                    return node
        return None

    def hash_func(self, telephone):
        return telephone % 100

    def print(self):
        for i in range(100):
            print(self.nodes[i][0].telephone)
        #print(self.nodes[81][0].telephone)






file = open('Table 22.txt')
file_lines = file.readlines()
lines = []
''''''
for i in range(1, len(file_lines)):
    file_lines[i] = file_lines[i].replace('\n', '').split('\t')
    lines.append(file_lines[i])

hash_table = HashTable()
telephones = []

for line in lines:
    hash_table.add(line)
    telephones.append(line[1])

start_time = time.time()
#for i in range(8000):
#    id = random.randrange(0, len(telephones))
#    hash_table.find_delete(telephones[id], True)
end_time = time.time() - start_time

hash_table.add(['привет', 4443430])
hash_table.add(['привет', 4441430])
hash_table.print()

print('seconds: ' + str(end_time))

