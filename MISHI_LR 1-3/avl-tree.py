from graphviz import Digraph
import random
import time


class Node:
    def __init__(self, id, telephone, abonent, left_child=None, right_child=None, parent=None):
        self.id = int(id)
        self.telephone = int(telephone) # 1
        self.abonent = abonent          # 0
        self.left_child = left_child
        self.right_child = right_child
        self.parent = parent
        self.balance = 0
        self.height = 0
        
    def change_child(self, old_child, new_child):
        if self.right_child is old_child:
            self.right_child = new_child
        if self.left_child is old_child:
            self.left_child = new_child


class AVLTree:
    nodes = [Node]

    def get_max_node_telephone(self):
        telephone = self.nodes[0].telephone
        for element in self.nodes:
            if element.telephone > telephone:
                telephone = element
        return telephone

    def get_min_node_telephone(self):
        telephone = self.nodes[0].telephone
        for element in self.nodes:
            if element.telephone < telephone:
                telephone = element
        return telephone

    def __init__(self, node):
        self.nodes[0] = node

    def get_root_node(self):
        temp = self.nodes[0]
        while temp.parent is not None:
            temp = temp.parent
        return temp

    def print_info_about_balance(self):
        temp = False
        temp2 = 0
        for node in self.nodes:
            if node.balance > 1 or node.balance < -1:
                temp = True
            if node.parent is None:
                temp2 += 1

        if temp is True:
            print('Есть несбалансированная нода')
        else:
            print('Нет несбалансированных нод')

        if temp2 > 1:
            print('Есть ноды без родителя')
        else:
            print('Нет нод без родителя')

        cycle = False
        for node in self.nodes:
            if node.right_child is node or node.left_child is node:
                cycle = True
                break
        if not cycle:
            print('Цикла нет')
        else:
            print('Цикл есть')
    def recalculate_balance(self, node):
        while node is not None:
            # выставление высоты
            if node.left_child is not None:
                node.height = node.left_child.height + 1
            if node.right_child is not None:
                if node.left_child is not None:
                    if node.right_child.height > node.left_child.height:
                        node.height = node.right_child.height + 1
                    else: node.height = node.left_child.height + 1
                else: node.height = node.right_child.height + 1

            # выставление баланса
            if node.left_child is not None:
                if node.right_child is not None:
                    node.balance = node.right_child.height - node.left_child.height
                else: node.balance = node.height * -1
            else:
                if node.right_child is not None:
                    node.balance = node.height
                else:
                    node.balance = 0
                    node.height = 0

            node = node.parent

    def balance(self, node):
        # TODO 110-... что-то не так
        while node is not None:
            if node.balance > 1:
                if node.right_child.balance < 1:  # big left rotation
                    n3 = node
                    n2 = node.right_child
                    n1 = node.right_child.left_child
                    c = None
                    if n1 is not None:
                        if n1.right_child is not None:
                            c = n1.right_child

                    n3.right_child = n1
                    if n1 is not None:
                        n3.right_child.parent = n3

                    if n1 is not None:
                        if n1.right_child is not None:
                            n1.right_child = n2
                            n1.right_child.parent = n1

                    n2.left_child = c
                    if c is not None:
                        n2.left_child.parent = n2
                    self.recalculate_balance(n2)

                self.left_rotation(node)

            if node.balance < -1:
                if node.left_child.balance > 1:  # big right rotation
                    n3 = node
                    n2 = node.left_child
                    n1 = node.left_child.right_child

                    b = None
                    if n1.left_child is not None:
                        b = n1.left_child

                    n3.left_child = n1
                    n3.left_child.parent = n3

                    n1.left_child = n2
                    n1.left_child.parent = n1

                    n2.right_child = b
                    if b is not None:
                        n2.right_child.parent = n2

                    self.recalculate_balance(n2)

                self.right_rotation(node)

            node = node.parent

    def right_rotation(self, node):
        n3 = node
        n1 = node.left_child
        c = None
        if n1.right_child is not None:
            c = n1.right_child

        if n3.parent is not None:
            n3.parent.change_child(n3, n1)
            n1.parent = n3.parent
        else:
            n1.parent = None

        n3.parent = n1
        n1.right_child = n3

        n3.left_child = c
        self.recalculate_balance(n3)

    def left_rotation(self, node):
        n3 = node
        n1 = node.right_child
        b = None
        if n1 is not None:
            if n1.left_child is not None:
                b = n1.left_child

        if n3.parent is not None:
            n3.parent.change_child(n3, n1)
            if n1 is not None:
                n1.parent = n3.parent
        else:
            n1.parent = None

        n3.parent = n1
        if n1 is not None:
            n1.left_child = n3

        n3.right_child = b
        self.recalculate_balance(n3)

    def add_node(self, node):
        root_node = self.get_root_node()
        while node.parent is None:
            if node.telephone >= root_node.telephone:
                if root_node.right_child is None:
                    root_node.right_child = node
                    node.parent = root_node
                else:
                    root_node = root_node.right_child
            elif node.telephone < root_node.telephone:
                if root_node.left_child is None:
                    root_node.left_child = node
                    node.parent = root_node
                else:
                    root_node = root_node.left_child
            else:
                root_node.right_child = node

        self.nodes.append(node)
        self.recalculate_balance(node)
        self.balance(node)

    def search_node(self, node_telephone):
        node_telephone = int(node_telephone)
        node = self.get_root_node()
        while node is not None:
            if node.telephone == node_telephone:
                return node
            elif node.telephone < node_telephone:
                if node.right_child is None:
                    node = None
                else:
                    node = node.right_child
            elif node.telephone > node_telephone:
                if node.left_child is None:
                    node = None
                else:
                    node = node.left_child
        return node

    def delete_node(self, node_telephone):
        node = self.search_node(node_telephone)

        if node is not None:
            if node.left_child is None:
                if node.parent is not None:
                    node.parent.change_child(node, node.right_child)
                if node.right_child is not None:
                    node.right_child.parent = node.parent
            elif node.right_child is None:
                node.parent.change_child(node, node.left_child)
                node.left_child.parent = node.parent
            else:
                node_child = node.left_child
                while node_child.right_child is not None or node_child.left_child is not None:
                    if node_child.left_child is not None:
                        if node_child.left_child.telephone < node_child.telephone:
                            node_child = node_child.left_child
                        else:
                            break
                    elif node_child.right_child is not None:
                        if node_child.right_child.telephone < node_child.telephone:
                            node_child = node_child.right_child
                        else:
                            break

                node_child_old_parent = node_child
                if node_child.parent is not node:
                    node_child_old_parent = node_child.parent

                if node.left_child is not None:
                    node_child.left_child = node.left_child
                    node_child.right_child = node.right_child
                if node.right_child is not None:
                    node_child.left_child.parent = node_child
                    node_child.right_child.parent = node_child

                node_child.parent.change_child(node_child, None)
                node_child.parent = node.parent
                if node.parent is not None:
                    node.parent.change_child(node, node_child)

                self.nodes.remove(node)
                self.recalculate_balance(node_child_old_parent)
                self.balance(node_child_old_parent)


    def generate_graph(self, temp=False):
        dot = Digraph()
        for node in self.nodes:
            dot.node(str(node.id),
                     str(node.telephone) +
                     #' \n' + str(node.abonent) +
                     ' \nb:(' + str(node.balance) + ')' +
                     ' \nh:(' + str(node.height) + ')')
            if node.left_child is not None:
                dot.edge(str(node.id), str(node.left_child.id), label='L')
            if node.right_child is not None:
                dot.edge(str(node.id), str(node.right_child.id), label='R')
        if temp is True:
            dot.render('avl-tree.gv', view=True)
        else:
            dot.render('avl-tree.gv', renderer=None)
        del dot


file = open('Table 22.txt')
file_lines = file.readlines()
for i in range(1, len(file_lines)):
    file_lines[i-1] = file_lines[i].replace('\n', '').split('\t')
    file_lines[i-1].append('0')


print('Добавление нод', end='\n\n')
avl_tree = AVLTree(Node(0, file_lines[0][1], file_lines[0][0]))

# for i in range(1, len(file_lines)-1):
for i in range(1, 1000):
    avl_tree.add_node(Node(i, file_lines[i][1], file_lines[i][0]))

avl_tree.generate_graph(True)

telephones = []
for node in avl_tree.nodes:
    telephones.append(node.telephone)


start_time = time.time()
i=0
for element in telephones:
    temp_telephone = random.choice(telephones)
    avl_tree.delete_node(temp_telephone)
    telephones.remove(temp_telephone)

end_time = time.time() - start_time
#avl_tree.generate_graph(True)

print('seconds: ' + str(end_time))
