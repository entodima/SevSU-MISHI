import time, random

class BTree(object):

  class Node(object):
    def __init__(self, t):
      self.keys = []
      self.children = []
      self.leaf = True
      self._t = t

    def split(self, parent, payload):
      new_node = self.__class__(self._t)

      mid_point = self.size//2
      split_value = self.keys[mid_point]
      parent.add_key(split_value)

      new_node.children = self.children[mid_point + 1:]
      self.children = self.children[:mid_point + 1]
      new_node.keys = self.keys[mid_point+1:]
      self.keys = self.keys[:mid_point]

      if len(new_node.children) > 0:
        new_node.leaf = False

      parent.children = parent.add_child(new_node)
      if payload < split_value:
        return self
      else:
        return new_node

    @property
    def _is_full(self):
      return self.size == 2 * self._t - 1

    @property
    def size(self):
      return len(self.keys)

    def add_key(self, value):
      self.keys.append(value)
      self.keys.sort()

    def add_child(self, new_node):
      i = len(self.children) - 1
      while i >= 0 and self.children[i].keys[0] > new_node.keys[0]:
        i -= 1
      return self.children[:i + 1]+ [new_node] + self.children[i + 1:]

  def __init__(self, t):
    self._t = t
    if self._t <= 1:
      raise ValueError("B-Tree must have a degree of 2 or more.")
    self.root = self.Node(t)

  def insert(self, payload):
    node = self.root
    if node._is_full:
      new_root = self.Node(self._t)
      new_root.children.append(self.root)
      new_root.leaf = False

      node = node.split(new_root, payload)
      self.root = new_root
    while not node.leaf:
      i = node.size - 1
      while i > 0 and payload < node.keys[i] :
        i -= 1
      if payload > node.keys[i]:
        i += 1

      next = node.children[i]
      if next._is_full:
        node = next.split(node, payload)
      else:
        node = next

    node.add_key(payload)

  def search(self, value, node=None):
    if node is None:
      node = self.root
    if value in node.keys:
      return True
    elif node.leaf:
      return False
    else:
      i = 0
      while i < node.size and value > node.keys[i]:
        i += 1
      return self.search(value, node.children[i])

  def print_order(self):
    this_level = [self.root]
    while this_level:
      next_level = []
      output = ""
      for node in this_level:
        if node.children:
          next_level.extend(node.children)
        output += str(node.keys) + " \t"
      print(output)
      this_level = next_level

b_tree = BTree(3)


file = open('Table 22.txt')
file_lines = file.readlines()
for i in range(1, len(file_lines)):
    file_lines[i-1] = file_lines[i].replace('\n', '').split('\t')
    file_lines[i-1].append('0')


telephones = []

start_time = time.time()
#for i in range(1, len(file_lines)-1):
for i in range(1, 8000):
    b_tree.insert(int(file_lines[i][1]))
    #telephones.append(int(file_lines[i][1]))

print(time.time()-start_time)

b_tree.print_order()
pass
#for element in telephones:
#    temp_telephone = random.choice(telephones)
#    b_tree.search(temp_telephone)
#    telephones.remove(temp_telephone)