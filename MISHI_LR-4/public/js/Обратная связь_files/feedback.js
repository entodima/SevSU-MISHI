function checkForm(form){
    var checked = {
        'name': false,
        'email':false,
        'phone':false,
        'comment': false,
        'file':false,
    };

    if(form.name.value.length>0) checked['name']=true;

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    checked['email']=re.test(String(form.email.value).toLowerCase());

    var phone = form.phone.value
    form.phone.value=phone.replace(/[^\d]/g,'').substr(0,11)
    if (form.phone.value.length <= 11) checked['phone'] = true;

    if(form.comment.value.length >0) checked['comment'] = true;

    if(form.file.files.length >0) checked['file'] = true;
    
    for(var key in checked){
        if(!checked[key]){
            console.log(key);
            document.getElementById('feedback_form-'.key).classList.add('error');
        }
    }
}

function showSubmit(){
    document.getElementById('feedback_form-submit_button').style.display="block";
}