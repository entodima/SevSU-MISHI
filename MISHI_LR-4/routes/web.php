<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/custom', 'IndexController@custom')->name('custom');
Route::post('/custom', 'IndexController@custom_send')->name('custom_send');

Route::get('/sharding', 'IndexController@sharding')->name('sharding');
Route::post('/sharding', 'IndexController@sharding_send')->name('sharding_send');