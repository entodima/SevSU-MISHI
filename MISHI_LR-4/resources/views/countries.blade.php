<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Страны</title>

    <style>
        p{
            font-family: -apple-system, BlinkMacSystemFont, sans-serif;
        }
        td{
            font-family: -apple-system, BlinkMacSystemFont, sans-serif;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <td><b>N</b></td>
                <td><b>Без разбиения</b></td>
                <td><b>С разбиением</b></td>
            </tr>
        </thead>
        @foreach($times_results as $times_result)
        <tr>
            <td>{{ $times_result['n'] }}</td>
            <td>{{ $times_result['t1'] }}</td>
            <td>{{ $times_result['t2'] }}</td>
        </tr>
        @endforeach
    </table>
</body>
</html>