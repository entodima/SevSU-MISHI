<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Шардинг - ajax</title>
    <script src="/js/jquery-3.4.1.min.js"></script>

    <style>
        h3{
            font-family: -apple-system, BlinkMacSystemFont, sans-serif;
        }
        p{
            font-family: -apple-system, BlinkMacSystemFont, sans-serif;
        }
        td{
            font-family: -apple-system, BlinkMacSystemFont, sans-serif;
        }
        #errorInfo{
            color: darkred;
        }
    </style>

    <script>
        let speed = 200;
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        function send(form) {
            var tBody = document.getElementById('tBody');
            if(checkN(form.n.value)){
                $.ajax({
                    type:'POST',
                    url:'{{ route('sharding_send') }}',
                    data: $(form).serialize(),
                    success:function(data){
                        document.getElementById('tableInfo').innerHTML = 'База данных ' + data.n;
                        tBody.innerText = '';
                        data.countries.forEach(function (country) {
                            var newRow = tBody.insertRow();
                            var newColumn0 = newRow.insertCell(0);
                            var newColumn1 = newRow.insertCell(1);
                            newColumn0.innerHTML = country['title'];
                            newColumn1.innerHTML = country['area']
                        });

                        form.n.value = '';
                        checkN();
                    }, error:function () {
                        console.log('error');
                    }
                });
            } else {
                showError(form.n.value + ' - не число');
            }
        }

        function checkN(value) {
            if(Number.isInteger(parseInt(value))){
                $('#submit').fadeIn(speed);
                showError('', true);
                return true;
            } else {
                if(value !== ''){
                    $('#submit').fadeOut(speed);
                    showError(value + ' - не число');
                    return false;
                }

            }
        }

        function showError(string='', hide = false) {
            var errorInfo = document.getElementById('errorInfo');
            errorInfo.innerHTML = string;
            if (!hide){
                $(errorInfo).fadeIn(speed);
            } else {
                $(errorInfo).fadeOut(speed);
            }

        }
    </script>
</head>
<body>
    <h3 id="tableInfo">-</h3>
        <table id="table">
            <thead>
                <tr>
                    <td><b>Страна</b></td>
                    <td><b>Площадь</b></td>
                </tr>
            </thead>
            <tbody id="tBody">
                <tr>
                    <td>-</td>
                    <td>-</td>
                </tr>
            </tbody>
        </table>

    <form method="post" onsubmit="send(this); return false">
        <input name="n", placeholder="Номер БД" onkeyup="checkN(this.value)" id="n">
        <input type="submit" value="submit" style="display: none;" id="submit">
    </form>

<div>
    <p id="errorInfo" style="display: none"></p>
</div>

</body>
</html>