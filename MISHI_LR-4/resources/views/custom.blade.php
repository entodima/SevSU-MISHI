<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Страны - ajax</title>
    <script src="/js/jquery-3.4.1.min.js"></script>

    <style>
        p{
            font-family: -apple-system, BlinkMacSystemFont, sans-serif;
        }
        td{
            font-family: -apple-system, BlinkMacSystemFont, sans-serif;
        }
        #errorInfo{
            color: darkred;
        }
    </style>

    <script>
        let speed = 200;
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        function send(form) {
            if(checkN(form.n.value)){
                $.ajax({
                    type:'POST',
                    url:'{{ route('custom_send') }}',
                    data: $(form).serialize(),
                    success:function(data){
                        document.getElementById('tBody').innerHTML = '<tr>' +
                            '<td>' + data.n + '</td>' +
                            '<td>' + data.time1 + '</td>' +
                            '<td>' + data.time2 + '</td>' +
                            '</tr>';
                        form.n.value = '';
                        checkN();
                    }, error:function () {
                        console.log('error');
                    }
                });
            } else {
                showError(form.n.value + ' - не число');
            }
        }

        function checkN(value) {
            if(Number.isInteger(parseInt(value))){
                $('#submit').fadeIn(speed);
                showError('', true);
                return true;
            } else {
                if(value !== ''){
                    $('#submit').fadeOut(speed);
                    showError(value + ' - не число');
                    return false;
                }

            }
        }

        function showError(string='', hide = false) {
            var errorInfo = document.getElementById('errorInfo');
            errorInfo.innerHTML = string;
            if (!hide){
                $(errorInfo).fadeIn(speed);
            } else {
                $(errorInfo).fadeOut(speed);
            }

        }
    </script>
</head>
<body>
        <table>
            <thead>
                <tr>
                    <td><b>N</b></td>
                    <td><b>Без разбиения</b></td>
                    <td><b>С разбиением</b></td>
                </tr>
            </thead>
            <tbody id="tBody">
                <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            </tbody>
        </table>

    <form method="post" onsubmit="send(this); return false">
        <input name="n", placeholder="n" onkeyup="checkN(this.value)" id="n">
        <input type="submit" value="submit" style="display: none;" id="submit">
    </form>

<div>
    <p id="errorInfo" style="display: none"></p>
</div>

</body>
</html>