<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Country;
use App\PartCountry;

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

class IndexController extends Controller
{
    public function index(){
        $times = [100, 500, 1000, 2000];
        $times_results = [];
        foreach ($times as $time){
            try {
                DB::connection()->getPdo();
            } catch (\Exception $e) {
                die("Could not connect to the database.  Please check your configuration. error:" . $e );
            }

            Country::query()->truncate();
            PartCountry::query()->truncate();
            DB::statement('CALL generate_data('.$time.',0);');

            $time1 = microtime_float();
            Country::limit(1);
            $time1 = microtime_float() - $time1;

            $time2 = microtime_float();
            PartCountry::limit(1);
            $time2  =  microtime_float()  - $time2;

            array_push($times_results, ['n'=>$time, 't1'=>$time1, 't2'=>$time2]);
        }

        return view('countries', compact(
            'times_results'));
    }

    public function custom(){


        return view('custom');
    }

    public function custom_send(Request $request){
        if (request()->ajax()){
            Country::query()->truncate();
            PartCountry::query()->truncate();

            DB::statement('CALL generate_data('.$request->input('n').',0);');

            $time1 = microtime(true );
            Country::limit(1);
            $time1 = microtime(true) - $time1;

            $time2 = microtime(true );
            PartCountry::limit(1);
            $time2  =  microtime(true)  - $time2;

            return response()->json([
                'success'=>'Готово',
                'n'=>$request->input('n'),
                'time1'=>$time1,
                'time2'=>$time2,
            ]);
        }
    }

    public function sharding(){
        return view('sharding');
    }

    public function sharding_send(Request $request){
        if (request()->ajax()){
            $coutnries = Country::on('mysql'.$request->n)->get();

            return response()->json([
                'success'=>'Готово',
                'n'=>$request->input('n'),
                'countries' => $coutnries,
            ]);
        }
    }
}